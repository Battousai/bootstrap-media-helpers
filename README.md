# Bootstrap Media Helpers

Adds mixins to agilize media targeting.

## Installing with bower:

Run `bower i --save bootstrap-media-helpers=git@bitbucket.org:Battousai/bootstrap-media-helpers.git`
in your terminal

## Usage:

```
#!sass

.Page {
    border-right-width: 0;
    transform: translate3d(-100%, 0, 0);
    @include media-sm {
        transform: translate3d(-50%, 0, 0);
    }
}

@include media-below-md {
    body {
        color: #ff0;
    }
}

```
